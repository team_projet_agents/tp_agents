/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


import fr.irit.smac.amak.Agent;
import fr.irit.smac.lxplot.LxPlot;
import fr.irit.smac.lxplot.commons.ChartType;

/**
 *
 * @author alex
 */
public class AgentVolet extends Agent<MyAMAS, Salle> {

    private Salle env;

    public AgentVolet(MyAMAS amas, Salle environnement) {
        super(amas);
        env = environnement;
    }

    @Override
    protected void onPerceive() {
        // Nothing goes here as the perception of neighbors criticality is already made
        // by the framework
    }

    @Override
    protected void onDecideAndAct() {
        if (env.getCapteur() > env.getSeuil())
        {
            if (env.getApoule() == 0)
            {
            	if (env.getVolet() > 0)
            	{
            		env.setVolet(-1);
            	}
            }
        }
        else if (env.getCapteur() < env.getSeuil())
        {
            if (env.getVolet() < 100)
            {
                env.setVolet(1);
            }
        }
    }

    @Override
    protected void onUpdateRender() {
        LxPlot.getChart("Salle", ChartType.BAR).add(3, env.getVolet());
    }
}

