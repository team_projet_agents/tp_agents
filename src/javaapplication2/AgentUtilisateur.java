/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


import fr.irit.smac.amak.Agent;
import fr.irit.smac.lxplot.LxPlot;
import fr.irit.smac.lxplot.commons.ChartType;

import static java.lang.Math.abs;

import java.util.Random;

/**
 *
 * @author alex
 */
public class AgentUtilisateur extends Agent<MyAMAS, Salle> {

    private Salle env;
    private int satisfaction = 0;
    
    Random rand = new Random();

    public AgentUtilisateur(MyAMAS amas, Salle environnement) {
        super(amas);
        env = environnement;
    }

    @Override
    protected void onPerceive() {
    	// Nothing goes here as the perception of neighbors criticality is already made
        // by the framework
    }

    @Override
    protected void onDecideAndAct() {
    	satisfaction = 100 - abs (env.getSeuil() - env.getCapteur());
    	
    	if (rand.nextInt(2) == 1)
    	{
    		env.setSeuil(1);
    	}
    	else
    	{
    		env.setSeuil(-1);
    	}
    	System.out.println(env.getSeuil());
    }

    @Override
    protected void onUpdateRender() {
        LxPlot.getChart("Salle", ChartType.BAR).add(5, satisfaction);
    }
}

