/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


import fr.irit.smac.amak.Environment;
import fr.irit.smac.amak.Scheduling;
import fr.irit.smac.lxplot.LxPlot;
import fr.irit.smac.lxplot.commons.ChartType;

import static java.lang.Math.abs;

/**
 *
 * @author alex
 */
public class Salle extends Environment
{
    private int volet;
    private int ampoule;
    private int capteur;
    
    private int heure;
    private int min;
    private int sec;
    
    private int seuil = 75;

    public Salle()
    {
        super(Scheduling.DEFAULT);
    }

    @Override
    public void onInitialization ()
    {
        volet = 0;
        ampoule = 0;
        capteur = 0;
        
        heure = 0;
        min = 0;
        sec = 0;
    }
    
    @Override
    public void onCycle()
    {
        passTime();
        capteur = heureToLum() * volet / 100 + ampoule;
        LxPlot.getChart("Salle", ChartType.BAR).add(4, heure);
    }
    
    private void passTime ()
    {
    	if (sec < 59)
    	{
    		sec ++;
    	}
    	else if (min < 59)
        {
            min ++;
        }
        else if (heure < 23)
        {
            heure ++;
            min = 0;
        }
        else
        {
            heure = 0;
            min = 0;
        }
    }
    
    public int heureToLum ()
    {
        return 8 * (12 - abs(heure - 12));
    }
    
    private void checkCorrection ()
    {
        // Vérifie la correction des valeurs
        if (volet > 100)
        {
            volet = 100;
        }
        else if (volet < 0)
        {
            volet = 0;
        }
        if (ampoule > 100)
        {
            ampoule = 100;
        }
        else if (ampoule < 0)
        {
            ampoule = 0;
        }
        if (capteur > 100)
        {
            capteur = 100;
        }
        else if (capteur < 0)
        {
            capteur = 0;
        }
        if (seuil > 100)
        {
            seuil = 100;
        }
        else if (seuil < 0)
        {
            seuil = 0;
        }
    }

    public int getVolet()
    {
        return volet;
    }

    public int getApoule()
    {
        return ampoule;
    }

    public int getCapteur()
    {
        return capteur;
    }
    
    public int getSeuil()
    {
        return seuil;
    }

    public void setVolet(int n)
    {
        volet += n;
        checkCorrection ();
    }
    
    public void setAmpoule(int n)
    {
        ampoule += n;
        checkCorrection ();
    }
    
    public void setSeuil(int n)
    {
        seuil += n;
        checkCorrection ();
    }
}
