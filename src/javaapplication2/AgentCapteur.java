/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


import fr.irit.smac.amak.Agent;
import fr.irit.smac.lxplot.LxPlot;
import fr.irit.smac.lxplot.commons.ChartType;

/**
 *
 * @author alex
 */
public class AgentCapteur extends Agent<MyAMAS, Salle> {

    private Salle env;
    private int valeur;

    public AgentCapteur(MyAMAS amas, Salle environnement) {
        super(amas);
        env = environnement;
    }

    @Override
    protected void onPerceive() {
        // Nothing goes here as the perception of neighbors criticality is already made
        // by the framework
    }

    @Override
    protected void onDecideAndAct() {
    	valeur = env.getCapteur();
    }

    @Override
    protected void onUpdateRender() {
        LxPlot.getChart("Salle", ChartType.BAR).add(1, valeur);
    }
}

