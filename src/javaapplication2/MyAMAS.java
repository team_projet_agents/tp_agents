/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


import fr.irit.smac.amak.Amas;
import fr.irit.smac.amak.Scheduling;

/**
 *
 * @author alex
 */
public class MyAMAS extends Amas<Salle> {
    
    public AgentAmpoule a;
    public AgentVolet v;
    public AgentCapteur c;
    public AgentUtilisateur u;
    
    public MyAMAS(Salle env) {
        super(env, Scheduling.DEFAULT);
    }
    @Override
    protected void onInitialAgentsCreation() {
        a = new AgentAmpoule(this, getEnvironment());
        v = new AgentVolet(this, getEnvironment());
        u = new AgentUtilisateur(this, getEnvironment());
        c = new AgentCapteur(this, getEnvironment());
    }
    
}